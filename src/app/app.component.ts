import { Component } from '@angular/core';
import { Cart } from './cart';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public CartData:Cart[] = [

    {price:1000,image:'../assets/images/book2.png',title:'Charismatic Monks of Lanna Buddhism Edited',body:'Charismatic Monks of Lanna Buddhism Edited by Paul T. Cohen'},
    {price:450,image:'../assets/images/book1.png',title:'คนข้ามแดน',body:'คนข้ามแดน ความเรียงว่าด้วยมานุษยวิทยา โดย พัฒนา กิติอาษา (เขียน)....เบญจพร ดีขุนทด (บรรณาธิการ)'},
    {price:150,image:'../assets/images/book3.png',title:'ศิวิไลซ์ข้ามพรมแดน',body:'การเผยแพร่ศาสนาของม้ง โปรเตสแตนต์ในเอเชียอาคเนย์ โดย ประสิทธิ์ ลีปรีชา'},
    {price:350,image:'../assets/images/book4.png',title:'สุนัขตกอับ กิตติศัพท์ของช้าง',body:'โดย โอโตเม ไกลน์ ฮัทธีซิงทวิช จตุวรพฤกษ์ แปล'},
    {price:250,image:'../assets/images/book5.png',title:'รหัสวัฒนธรรม',body:'โดย สุวิชัย ทวันแก้ว'},
    {price:250,image:'../assets/images/book6.png',title:'วัฒนธรรม คือ อำนาจ ปฏิบัติการแห่งอำนาจ',body:'ตัวตน และชนชั้นใหม่ในพื้นที่วัฒนธรรม โดยชูศักดิ์ วิทยาภัค บรรณาธิการ.... เบญจพร ดีขุนทด และมาลี สิทธิเกรียงไกร (บรรณาธิการต้นฉบับ)'},
    {price:250,image:'../assets/images/book7.png',title:'ประวัติศาสตร์นอกกรอบรัฐชาติ 55 ปี ขบวนการกู้ชาติไทใหญ่',body:'โดย อัมพร จิรัฐติกร'},
    {price:120,image:'../assets/images/book8.jpg',title:'ตัวตนคนปะกาเกอะญอ',body:'โดย พรสุข เกิดสว่าง'},
    {price:130,image:'../assets/images/book9.png',title:'มลาบรีบนเส้นทางการพัฒนา',body:'โดย ศักรินทร์ ณ น่าน'},
    {price:700,image:'../assets/images/book10.png',title:'ทฤษฎีชาติพันธุ์สัมพันธ์',body:'รวมบทความทางทฤษฎีเกี่ยวกับอัตลักษณ์ เชื้อชาติ กลุ่มชาติพันธุ์ การจัดองค์กรความสัมพันธ์ทางชาติพันธุ์และรัฐประชาชาติ สุเทพ สุนทรเภสัช (แปล) ประสิทธิ์ ลีปรีชา (บรรณาธิการ)'},
    {price:200,image:'../assets/images/book11.png',title:'พิธีกรรมและปฏิบัติการในสังคมชาวนาภาคเหนือของประเทศไทย',body:'ชิเกฮารุ ทานาเบ (ผู้เขียน) ขวัญชีวัน บัวแดง และ อภิญญา เฟื่องฟูสกุล (บรรณาธิการ)'},
    {price:150,image:'../assets/images/book12.jpg',title:'เราคือ เตหน่ากู',body:'สุวิชาน พัฒนาไพรวัลย์(ชิ) เขียน มาลี สิทธิเกรียงไกร และ ถาวร กำพลกูล (บรรณาธิการ)'},
    {price:75,image:'../assets/images/book13.jpg',title:'มุสลิม : มายาคติทางประวัติศาสตร์และวัฒนธรรม',body:'มาลี สิทธิเกรียงไกร (บรรณาธิการ)'},
    {price:1000,image:'../assets/images/book20.png',title:'เส้น สี ชีวิต (COLOR, LINE AND LIFE)',body:'อัจฉริยะทางศิลปะของชาติพันธุ์ในแดนเหนือโดย พรสุข เกิดสว่าง'},
    {price:650,image:'../assets/images/book14.jpg',title:'Communities of Potential Social Assemblages in Thailand and Beyond',body:'Edited by Shigeharu Tanabe This book provides fresh ways of looking at community movements and social actors in Thailand, Cambodia, and Myanmar. The chapters cover a wide range of movements, from personal and social development based on Buddhist principles to community movements centered on other religious, spiritual, and traditional practices. '},
    {price:650,image:'../assets/images/book15.png',title:'Scholaarship And Engagement In Mainland Southeast Asia',body:'A festschrift in honor of Achan Chayan Vaddhanaphuti Edited by Oscar Salemink In collaboration with Patcharin Lapanun Benjaporn Deekhuntod Malee Sitthikriengkrai'},
    {price:1500,image:'../assets/images/book16.png',title:'Mobility and Heritage in Northern Thailand and Laos: Past and Present',body:'Edited by Olivier Evard Dominique Guillaud Chayan Vaddhanaphuti'},
    {price:250,image:'../assets/images/book17.png',title:'Islamic Identity in Chiang Mai City',body:'By Suthep Soonthornpasuch'},
    {price:250,image:'../assets/images/book21.jpg',title:'Stateless Rohingya... Running on Empty',body:'Photos by Suthep Kritsanavarin This series of photos reveals the true story of the world s most neglected population group, helping us to experience the story of those who are strangers in their own homeland. For the Rohingya, life is an endless struggle; they run from place to place in search of a land they can call home, yet end up running on empty.'},
    {price:0,image:'../assets/images/book18.png',title:'(ฟรี) รายงานประจำปีศูนย์ศึกษาชาติพันธุ์และการพัฒนา',body:'ประจำปี พ.ศ. 2552-2554'},
    {price:0,image:'../assets/images/book19.png',title:'(ฟรี) รายงานประจำปีศูนย์ศึกษาชาติพันธุ์และการพัฒนา',body:'ประจำปี พ.ศ. 2555-2560'},

  ];
  public tax:number = 7;
  getTotal(event)
    {
      console.log(event);
    }
}
